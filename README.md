# forge

A small toolkit for managing and rebuilding [NixOS] servers.

[NixOS]: https://nixos.org/nixos

It listens for update events via a small HTTP server
and uses the configuration in your repository to run deployments.

## Configuration

`forge` has two levels of configuration.
One is required to make the daemon run and react to update events,
the second is repository specific configuration and deploy secrets.

When running `forge` manually you need to provide a verification token
either via environment variable (`FORGE_TOKEN`) or a CLI argument.

Alternatively you can configure `forge` via the [`nixos`] module.
Note that you will have to manually do SSL termination
via an external http proxy.

[`nixos`]: https://github.com/spacekookie/kookiepkgs

```nix
services.forge = {
  enable = true;
  port = 9090;
  token = "<validation token here>";
};
```

Inside a repository `forge` depends on `forge.yml` as a configuration
that provides deploy commands and secrets. Check out [`examples/forge.yml`]
for an example configuration.

[`examples/forge.yml`]: examples/forge.yml

## Security Layout

```
+===========+            +===============+            +=================+
|   Token   | - blake2 - |   Encryption  |            |   Verification  |
+===========+            |     Key       | - blake2 - |     Token       |
                         +===============+            +=================+
```

* `Token` is the secret handled by your git server
* `Key` is the hashed token and used for encryption
* `Verification Token` is a hash of the `Key` which is used
  for request verification

The verification token can be kept in your global system configuration
because from it no other secrets can be derived. Other deploy secrets
can be kept in your configuration repository because they can be
encrypted with the key, derived from the secret token.

## Setup

`forgectl` provides some utilities to configure itself.
To generate a token you should use a tool such as `pwgen`.
Keep this token secret!

```console
$ export FORGE_TOKEN=$(pwgen 64)
$ echo $FORGE_TOKEN
thoj3oe5aeGaeciegieph5Paideuvoocoph7euSohvah6iP1AeY4thupae4ahsho
```

Now generate a verification token from the secret token.
You can embed this into your forge configuration.
Don't forget to generate a `$FORGE_SALT`

```
echo $FORGE_TOKEN | forgectl hash --salt $FORGE_SALT
amEDe7/DpJKuJN5nwC77rUsDZ1Vkam0bQadcosseHi8==0w0=aaJi5id1ooleeboh
```

At this point you can also setup deploy secrets. This could be used
for ssh keys to deploy locally.

```
$ ssh-keygen -t ed25519 -f tmp_key -N ''
Generating public/private ed25519 key pair.
The key's randomart image is:
+--[ED25519 256]--+
|        ..+OB    |
|       o..oBo*o  |
|      o.  + *++o |
|       ..  +.B o+|
|       .S.o = +o.|
|        o. +  .  |
|      +.o .    E.|
|     +.= .      +|
|    ..o.o      .o|
+----[SHA256]-----+
$ cat tmp_key | target/debug/forgectl encrypt -s (echo $FORGE_TOKEN)
LS0tCml2OiB0WnhMdVJiRWF0Z2luZU0zL2FHM1BYUGpxOEF1UWhtQlhuRWFlRG9raS9scjZ
5dUIrRWI0VW1mbFNDNm9SSWVkbGtkWlNZaXIwRnVFZjdpTm1GazFsUT09Cm5vbmNlOiB1U2
hOMExMdURIVGFrMXFWWUlaUEVmUS96RjFRSVo2Z29hRUVrZE11V3BiaUxmbjJTMmdMNkcyY
mErOWdXazVQaUZMTlN3dnZqT0tZb3VEdkVQZlkzQT09CmRhdGE6IEFFenc5QXVkSWZ1eDNO
THVKVGgrSFI4cXB6UmtiOWI5eHBGTTEvUUpiTW02TWM3ZDMybG9WM3lVdzRmK0dGY3B0Q1V
kQ0lJN3Q5YkdzNUh1M1p2aURZNzhESWFPaVV6ZU5Rd2lxN2g1TFZGc01mSUVQTks2SFI1cD
lsNjNPT20rcGdqdThod2F6UGpEK3hHMFBUQ3BoanhWS1JNNkg3UzF2d0JDRGY1azkvazBQU
ThPUGlsSDdLZ1hqWHdBUS9iWk1qQWRRbHAvWWh0TmcxR1ZtTVpjbXhaY2w3b1lNY1E4MFkz
R1VKeEpCU1RybDhrSlQ5OUhiNmRaejNTbGdEV215RUlmM1hFZWw3VWkxdUpKakxOdHdqL0l
RSWhUZittWU1LQnE3Yk1pTElDaTB3TXh5Zi9JUFFRY0NOYm44dGxWN1d6UHFPbmVtN0FEVj
N1L0Y1Z1Q0N25rZ2pSaytseUFCc25wejRteFBHVnhYNGtxNVU1cytlcVVuV3V4YmVnSE10N
294L21KZVJZRjJrblNFQjlIS1ZNNXVodVJBNlc2ZXFmWUw3M0g3bEtBc1p0bkdpcGxnWTR5
OVpDR0RaOVM1SWV2QVFYbWVadWhUSWVmaVV5MTVXTm5HOUw0REFsUFE1VTlyRTY5aTlPOUJ
mRkpFL0todTdPL1hqMEhDOGh4SW5CZzBReTlFRmh2Z0l5aXRPSENLZTJCM3F2ZHlnRnJuU0
VaT0FTbUt2NnB4ZlFiMFpzQURaQmMzZz09
```

Add the generated YAML to the `secrets` section of the `forge.yml` configuration.
