use keybob::Key;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs::File, io::Read};

mod utils {
    pub use crate::*;
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Auth {
    pub user: String,
    pub method: String,
    pub key: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Deploy {
    pub cmd: String,
    pub mirror: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub auth: Auth,
    pub deploy: Deploy,
    secrets: HashMap<String, String>,
}

impl Config {
    /// Open and sanitise a forge config
    pub fn load(path: &str) -> Option<Config> {
        let mut f = File::open(path).ok()?;
        let mut s = String::new();
        f.read_to_string(&mut s).ok()?;

        let yaml: Config = serde_yaml::from_str(&s).ok()?;
        let secrets = yaml
            .secrets
            .into_iter()
            .fold(HashMap::new(), |mut map, (k, v)| {
                map.insert(k, v.chars().filter(|c| c != &'\n').collect());
                map
            });

        Some(Config { secrets, ..yaml })
    }

    /// Parse what secret should be used for auth and loads it from the store
    pub fn parse_secret(&mut self, key: Key) -> String {
        let secret = match self.auth.key.as_str() {
            "inline" => "key",
            s => s,
        };

        let encrypted = utils::from_yaml(
            self.secrets
                .remove(secret)
                .expect(&format!("Required secret not found in hash: `{}`", secret)),
        );

        let decrypted = utils::decrypt(&key, encrypted);
        String::from_utf8(decrypted).expect(&format!("Invalid UTF-8 in secret: `{}`", secret))
    }
}
