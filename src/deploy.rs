//! Deployment tools for `forge`

use crate::{config::Config, derive_key};

use serde_json::Value;
use std::{
    collections::HashMap,
    fs::{remove_dir_all, remove_file, File},
    io::Write,
    process::{Command, Stdio},
};
use tempdir::TempDir;

type JsonMap = HashMap<String, Value>;

pub struct DeployData {
    pub secret: String,
    pub salt: String,
    pub repo_key: String,
    pub json: JsonMap,
}

pub fn run(data: DeployData) {
    println!("Running deploy request");

    let repo = get_repo(&data.json);
    let dir = TempDir::new("forge").expect("Failed to create temp working dir!");
    let dir_str = get_tmp_path(&dir);
    println!("Created temp dir {}", dir_str);

    unsafe {
        let mut bad: &mut [u8; 2] = std::mem::uninitialized();
        *bad = [1, 2];
    }

    // Format paths required
    let git_dir = format!("{}/configurations", dir_str);
    let deploy_key_path = format!("{}/fucking_delete_me.priv", dir_str);
    let run_script = format!("{}/forge_run.sh", git_dir);

    run_command(
        &dir_str,
        "ssh-agent",
        &[
            "bash",
            "-c",
            &format!(
                "'ssh-add {}; git clone --recursive {} configurations'",
                data.repo_key, repo
            ),
        ],
    );

    let mut config =
        Config::load((git_dir.clone() + "/.forge.yml").as_str()).expect("Invalid config!");
    let key = derive_key(data.secret, data.salt);

    // Run this to pre-cache a generation
    run_command(
        &git_dir,
        "nixos-rebuild",
        &["build", "-I", "nixos-config=configuration.nix"],
    );

    let cmd = &config.deploy.cmd;
    File::create(&run_script)
        .and_then(|mut f| f.write_all(b"#!/bin/sh\n").map(|_| f))
        .and_then(|mut f| f.write_all(cmd.as_bytes()))
        .expect("Failed to build system config!");

    // !!! WARNING: writing sensitive key material to disk
    let deploy_key = config.parse_secret(key);
    File::create(&deploy_key_path)
        .and_then(|mut f| f.write_all(deploy_key.as_bytes()))
        .expect("Failed to build system config!");

    // Load required data from config
    let user = config.auth.key;
    let mirror = config.deploy.mirror;

    eprintln!("Key `auth.method` currently ignored!");

    // Clear the mirror directory first
    let mut child = Command::new("ssh")
        .args(&[
            &format!("{}@localhost -i {}", user, deploy_key_path),
            "bash",
            &run_script,
        ])
        .env("OUT_DIR", &git_dir)
        .env("MIRROR", mirror)
        .current_dir(&git_dir)
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn()
        .map_err(|e| scrub(&deploy_key_path).map(|_| e).unwrap())
        .expect("Failed to run git-clone command!");
    child.wait().unwrap();

    // Remove working state
    scrub(&deploy_key_path);
    remove_dir_all(&dir).unwrap();
}

/// Try to clear the private keyfile from disk
fn scrub(path: &String) -> Option<()> {
    remove_file(path).expect("CRITICAL: Failed to delete private key from temp folder!!");
    Some(())
}

fn get_repo(json: &JsonMap) -> String {
    match json.get("project") {
        Some(Value::Object(o)) => match o.get("git_ssh_url") {
            Some(Value::String(s)) => s.clone(),
            _ => panic!("Malformed json body!"),
        },
        _ => panic!("Malformed json body!"),
    }
}

fn get_tmp_path(dir: &TempDir) -> String {
    dir.path()
        .as_os_str()
        .to_str()
        .expect("Failed to format deploy key path!")
        .to_owned()
}

/// Run a command while streaming it's stdout and stderr
fn run_command(pwd: &str, cmd: &str, args: &[&str]) {
    println!("Running: `{} {} ...`", cmd, args[0]);

    let mut child = Command::new(&cmd)
        .args(args)
        .current_dir(pwd)
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap();

    child.wait().expect(&format!("Failed to run {}", cmd));
}
