use clap::ArgMatches;
use std::io::{stdin, Read};

mod utils {
    pub use crate::*;
}

/// The module version
pub const VERSION: &'static str = "0.1.0";

pub fn run(args: &ArgMatches<'_>) {
    let salt: String = args.value_of("SALT").unwrap().trim().into();

    // Read token from stdin
    let mut data = Vec::new();
    stdin()
        .read_to_end(&mut data)
        .expect("Failed to read from STDIN");
    let string = String::from_utf8(data)
        .unwrap_or_else(|_| utils::log_fatal("Provided invalid UTF-8 data!"));

    // Hash token twice with random salt
    let token = utils::secret_to_verify(string.trim().into(), salt.trim());
    println!("{}", token);
}
