use clap::{App, AppSettings, Arg, SubCommand};
use forge::{hash, secret, server, setup};

fn main() {
    let app = App::new("forge-cli")
        .version("0.1.0")
        .about("Management CLI for forge")
        .setting(AppSettings::SubcommandRequired)
        .setting(AppSettings::ColorAuto)
        .subcommand(
            SubCommand::with_name("encrypt")
                .about("Create embeddable configuration secrets")
                .version(secret::VERSION)
                .arg(
                    Arg::with_name("KEY")
                        .required(true)
                        .takes_value(true)
                        .short("k")
                        .long("key")
                        .help("A secret to use as a key"),
                )
                .arg(
                    Arg::with_name("SALT")
                        .required(true)
                        .takes_value(true)
                        .short("s")
                        .long("salt")
                        .help("The salt to use for hash operations"),
                ),
        )
        .subcommand(
            SubCommand::with_name("hash")
                .about("Use a secret token to crate a public verification token")
                .version(hash::VERSION)
                .arg(
                    Arg::with_name("SALT")
                        .required(true)
                        .takes_value(true)
                        .short("s")
                        .long("salt")
                        .help("The salt to use for hash operations"),
                ),
        )
        .subcommand(
            SubCommand::with_name("setup")
                .about("Generate a skeleton `forge.yml` configuration")
                .version(setup::VERSION)
                .arg(
                    Arg::with_name("file")
                        .short("f")
                        .long("file")
                        .help("Provide a file name"),
                ),
        )
        .subcommand(
            SubCommand::with_name("run")
                .about("Run the forge server")
                .version(server::VERSION),
        );

    match app.get_matches().subcommand() {
        ("hash", Some(m)) => hash::run(m),
        ("encrypt", Some(m)) => secret::run(m),
        ("setup", Some(m)) => setup::run(m),
        ("run", _) => server::run(),
        _ => unreachable!(),
    }
}
