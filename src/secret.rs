//! The `secret` utility module which hashes tokens and encrypts user keys

use clap::ArgMatches;
use std::io::{stdin, stdout, Read, Write};

mod utils {
    pub use crate::*;
}

/// The module version
pub const VERSION: &'static str = "0.1.0";

/// Run the `secret` module
pub fn run(args: &ArgMatches<'_>) {
    let secret = args.value_of("KEY").unwrap().into();
    let salt = args.value_of("SALT").unwrap().into();

    let mut data = Vec::new();
    stdin()
        .read_to_end(&mut data)
        .expect("Failed to read from STDIN");

    /* Create key from secret, then encrypt DATA */
    let key = utils::derive_key(secret, salt);
    let encrypted = utils::encrypt(&key, data);
    let encoded = utils::to_yaml(encrypted);

    stdout()
        .write_all(encoded.as_bytes())
        .expect("Failed to write to STDOUT");
}
