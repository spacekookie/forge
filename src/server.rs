use actix_web::{
    middleware,
    web::{self, Json},
    App, HttpRequest, HttpResponse, HttpServer,
};
use serde_json::Value;
use std::{collections::HashMap, env, thread};

mod utils {
    pub use crate::*;
}

/// The module version
pub const VERSION: &'static str = "0.1.0";

#[derive(Clone)]
struct Params {
    bind: String,
    token: String,
    salt: String,
    key: String,
}

impl Params {
    pub fn new() -> Self {
        Self {
            bind: env::var("FORGE_BIND")
                .unwrap_or("0.0.0.0:12220".into())
                .into(),
            token: env::var("FORGE_TOKEN")
                .expect("Value `FORGE_TOKEN` missing")
                .into(),
            salt: env::var("FORGE_TOKEN_SALT")
                .expect("Value `FORGE_TOKEN_SALT` missing")
                .into(),
            key: env::var("FORGE_DEPLOY_KEY")
                .expect("Value `FORGE_DEPLOY_KEY` missing")
                .into(),
        }
    }
}

/// simple handle
fn index(
    state: web::Data<Params>,
    (json, req): (Json<Option<HashMap<String, Value>>>, HttpRequest),
) -> HttpResponse {
    let h = req.headers();
    let secret: String = h
        .get("x-gitlab-token")
        .expect("Request didn't include `x-gitlab-event`")
        .to_str()
        .unwrap()
        .into();

    let hash = utils::secret_to_verify(secret.trim().into(), state.salt.trim());
    let valid = utils::blake2_verify(state.token.clone(), hash);

    match (json.0, valid) {
        (Some(json), true) => {
            thread::spawn(move || {
                use utils::deploy::DeployData;
                utils::deploy::run(DeployData {
                    json,
                    secret,
                    salt: state.salt.clone(),
                    repo_key: state.key.clone(),
                });
            });
            HttpResponse::Ok().body(format!("Ok"))
        }
        _ => HttpResponse::Forbidden().body(format!("Failed to authenticate!")),
    }
}

pub fn run() {
    let params = Params::new();
    let bind = params.bind.clone();

    HttpServer::new(move || {
        App::new()
            .data(params.clone())
            .wrap(middleware::Logger::default())
            .service(web::resource("/").to(index))
    })
    .bind(&bind)
    .unwrap()
    .run()
    .unwrap();
}
