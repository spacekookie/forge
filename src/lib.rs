//! The `forge` library

pub mod hash;
pub mod secret;
pub mod setup;

pub mod config;
pub mod deploy;
pub mod server;

// use blake2::digest::{Input, VariableOutput};
use blake2::{Blake2s, Digest};

use keybob::{Key, KeyType};
use miscreant::siv::Aes256Siv;

use base64;
use serde::{Deserialize, Serialize};
use serde_yaml;
use textwrap::fill;

const BLAKE_16_LENGTH: usize = 32;
const SALT_DIVIDER: &'static str = "=0w0=";

/// A blake16 hash, consisting of a byte array and salt
pub struct Hash<'salt>([u8; BLAKE_16_LENGTH], &'salt str);

pub(crate) trait PrettyHash {
    fn to_string(self) -> String;
}

impl PrettyHash for (String, &str) {
    fn to_string(self) -> String {
        format!("{}{}{}", self.0, SALT_DIVIDER, self.1)
    }
}

impl<'salt> From<&'salt String> for Hash<'salt> {
    fn from(s: &'salt String) -> Self {
        let split: Vec<_> = s.split(SALT_DIVIDER).collect();
        let hash = split.get(0).unwrap().as_bytes().into_iter().zip(0..).fold(
            [0; BLAKE_16_LENGTH],
            |mut acc, (c, i)| {
                acc[i] = *c;
                acc
            },
        );
        let salt = split.get(1).unwrap();

        Hash(hash, salt)
    }
}

/// Some enrypted data
#[derive(Debug)]
pub struct PackedData {
    pub nonce: Vec<u8>,
    pub iv: Vec<u8>,
    pub data: Vec<u8>,
}

/// Small utility to print nice user messags and exit
fn log_fatal(msg: &str) -> ! {
    eprintln!("{}", msg);
    std::process::exit(2);
}

/// Hash a salted value with blake2
pub fn blake2<'salt>(data: &str, salt: &'salt str) -> Hash<'salt> {
    let mut hasher = Blake2s::new();

    let to_hash = format!("{}{}{}", data, SALT_DIVIDER, salt);
    hasher.input(to_hash.as_bytes());

    let buffer = hasher.result().as_slice().iter().zip(0..).fold(
        [0; BLAKE_16_LENGTH],
        |mut slice, (x, i)| {
            slice[i] = *x;
            slice
        },
    );

    Hash(buffer, salt)
}

/// Verify two strings in always constant time
pub fn blake2_verify(a: String, b: String) -> bool {
    a.chars()
        .zip(b.chars())
        .fold(true, |acc, (a, b)| (a == b) && acc)
}

/// A utility to hash a secret twice so it can be compared
pub fn secret_to_verify(secret: String, salt: &str) -> String {
    let h1 = blake2(secret.as_str(), salt);
    let h1_enc = base64_encode(&h1.0.iter().map(|i| *i).collect());

    let h2 = blake2(h1_enc.as_str(), h1.1);
    let h2_enc = base64_encode(&h2.0.iter().map(|i| *i).collect());

    (h2_enc, salt).to_string()
}

/// Derive a key from a secret token
pub fn derive_key(secret: String, salt: String) -> Key {
    Key::from_pw(KeyType::Aes256, secret.as_str(), salt.as_str())
}

/// Decrypt some packed secret
pub fn decrypt(key: &Key, data: PackedData) -> Vec<u8> {
    let mut ctx = Aes256Siv::new(key.as_slice());
    ctx.open(
        vec![data.nonce.as_slice(), data.iv.as_slice()],
        data.data.as_slice(),
    )
    .unwrap()
}

/// Encrypt some data into a packed secret
pub fn encrypt(key: &Key, data: Vec<u8>) -> PackedData {
    let mut ctx = Aes256Siv::new(key.as_slice());
    let iv = Key::new(KeyType::Aes256);
    let nonce = Key::new(KeyType::Aes256);
    let ciph = ctx.seal(vec![nonce.as_slice(), iv.as_slice()], data.as_slice());

    PackedData {
        iv: iv.as_slice().into(),
        nonce: nonce.as_slice().into(),
        data: ciph,
    }
}

/// Encode a piece of arbitary data into a bse64 string
pub fn base64_encode(data: &Vec<u8>) -> String {
    return base64::encode(data);
}

/// Decode a base64 string into arbitrary data
pub fn base64_decode(data: &String) -> Vec<u8> {
    return base64::decode(data).unwrap();
}

/// Same as `PackedData` but all bas64 encoded because readability
#[derive(Serialize, Deserialize, Debug)]
pub struct YamlData {
    iv: String,
    nonce: String,
    data: String,
}

impl From<PackedData> for YamlData {
    fn from(pd: PackedData) -> Self {
        Self {
            iv: base64_encode(&pd.iv),
            nonce: base64_encode(&pd.nonce),
            data: base64_encode(&pd.data),
        }
    }
}

impl From<YamlData> for PackedData {
    fn from(pd: YamlData) -> Self {
        Self {
            iv: base64_decode(&pd.iv),
            nonce: base64_decode(&pd.nonce),
            data: base64_decode(&pd.data),
        }
    }
}

pub fn to_yaml(pd: PackedData) -> String {
    let yd: YamlData = pd.into();
    let s = serde_yaml::to_string(&yd).unwrap();
    format!("{}", fill(&base64_encode(&s.into_bytes()), 72))
}

pub fn from_yaml(yaml: String) -> PackedData {
    let stripped: String = String::from_utf8(
        yaml.into_bytes()
            .into_iter()
            .map(|c| {
                println!("Char: `{}`", c);
                c
            })
            .collect(),
    )
    .unwrap();
    let tmp = base64_decode(&stripped);
    let yaml = String::from_utf8(tmp).unwrap();
    let yd: YamlData = serde_yaml::from_str(&yaml).unwrap();
    yd.into()
}
