use clap::ArgMatches;

/// The module version
pub const VERSION: &'static str = "0.1.0";

pub fn run(args: &ArgMatches<'_>) {
    let _: String = args.value_of("FILE").unwrap().into();
    unimplemented!()
}
